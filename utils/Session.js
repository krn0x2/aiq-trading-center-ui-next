import cookie from 'react-cookie';

import { authServerConfig, AUTH_TOKEN_NAME } from '~/config/auth.config.js';


function getExpiryDate() {
  const tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  return tomorrow;
}


export const clearAuthToken = () => {
  cookie.remove(AUTH_TOKEN_NAME);
  cookie.remove('JSESSIONID');
};


export const getAuthToken = () => cookie.load(AUTH_TOKEN_NAME);


export const setAuthToken = (token) => {
  cookie.save(AUTH_TOKEN_NAME, token, {
    maxAge: 86400, // 24 hours in seconds.
    expires: getExpiryDate(),
  });
};


export const redirectToAuthServer = () => {
  const ref = window.location.href;
  let redirectUri = encodeURIComponent(ref);

  // Since we use a hashHistory, we need to identify whether the url contains any further
  // query params, if not then append "?" to ensure the react router (hashhistory) pass
  // appropriate query identifiers like: 'access_token', 'token_type' and not pass it without "?"
  if (ref.indexOf('&') === -1) {
    redirectUri += '?';
  }

  window.location.href = `${authServerConfig.protocol}://${authServerConfig.hostname
  }/oauth/authorize?grant_type=implicit&response_type=token&client_id=sip&redirect_uri=${redirectUri}`;
};

export const logoutUserRedirect = () => {
  window.location.href = `${authServerConfig.protocol}://${authServerConfig.hostname}/logout.do`;
};
