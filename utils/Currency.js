import { currencySymbols } from '../constants';

export default currency => currencySymbols[currency];

