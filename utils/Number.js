const currencyToLocale = {
  USD: 'en-US',
  EUR: 'de-DE',
  GBP: 'en-GB',
  CAD: 'en-US',
  AUD: 'en-AU',
  NZD: 'en-NZ',
  IDR: 'id-ID',
  SGD: 'en-GB',
  CHF: 'de-DE',
};

export default (number, currency) => {
  if (typeof (number) === 'number') {
    return number.toLocaleString(
      currencyToLocale[currency],
      { maximumFractionDigits: 2 },
    );
  }
  return '--';
};

export const localCurrency = (number, currency) => {
  if (typeof (number) === 'number') {
    return number.toLocaleString(
      currencyToLocale[currency],
      { style: 'currency', currency },
    );
  }
  return '--';
};

export const truncate = (number, precision = 2) => {
  if (typeof (number) === 'number') {
    return number.toFixed(precision);
  }
  return '--';
};


export const percentage = (num, den) => (num * 100) / den;
