export default (dateString) => {
  let date = dateString.dayOfMonth;
  if (date < 10) { date = `0${date}`; }
  let month = dateString.monthValue;
  if (month < 10) { month = `0${month}`; }
  const year = dateString.year;
  return `${year}-${month}-${date}`;
};
