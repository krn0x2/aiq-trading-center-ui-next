/* eslint-disable */
const express = require('express');
const next = require('next');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const fetch = require('isomorphic-unfetch');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare()
  .then(() => {
    const server = express();
    server.use(cookieParser());
    server.use(bodyParser.json());
    // server.get('/_next',(req,res) => handle(req, res));
    server.get('*', (req, res) => {
      handle(req, res);
      return;
      const redirect_uri = req.protocol + '://' + req.get('host') + req.originalUrl;
      console.log(redirect_uri);
      if(req.cookies.token)
        return handle(req, res);
      const referer = req.headers.referer;
      const authUrl = referer+'tc/user/api/session/';
      console.log(req.originalUrl);
      fetch('http://localhost.mediaiqdigital.com/tc/user/api/session/', {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${req.cookies.token}`,
        },
      }).then((response) => {

        switch (response.status) {
          case 401:
            res.redirect('//accounts.mediaiqdigital.com/oauth/authorize?grant_type=implicit&response_type=token&client_id=aiq-ui&redirect_uri='+redirect_uri);
            break;
          case 403:
            // delete $cookies['token'];
            // window.location.href = $AUTH_SERVER + '/403.jsp';
            // break;
        }
        // console.log(response.status,response.text(),response.json());
        //return r.json();
      });
    });
    server.listen(3000, 'localhost.mediaiqdigital.com', (err) => {
      if (err) throw err;
      console.log('> Ready on http://localhost:3000');
    });
  })
  .catch((ex) => {
    console.error(ex.stack);
    process.exit(1);
  });
