import React from 'react';
import Link from 'next/link';
import { Layout, Menu } from 'antd';

const { Header } = Layout;
const tabs = [
  'CAMPAIGNS',
  'ADVERTISERS',
  'BOOKINGS',
  'IO',
  'ANALYTICS',
  'SEGMENTS',
];

export default class JarvisHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 3,
    };
  }

  render() {
    return (
      <Header style={{ position: 'fixed', width: '100%', zIndex: 1 }}>
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={['2']}
        >
          <Menu.Item key={'tab'}>
            <Link href="/">
              <div style={{ width: '48px',
                height: '48px',
                backgroundImage: 'url(\'/static/jarvis.svg\')',
                backgroundPosition: 'center',
                backgroundRepeat: 'no-repeat' }}
              />
            </Link>
          </Menu.Item>
          {
            tabs.map(tab => (
              <Menu.Item key={tab}>
                <Link href={tab.toLowerCase()}>
                  <a>{tab}</a>
                </Link>
              </Menu.Item>
            ))
          }
        </Menu>
      </Header>
    );
  }
}
