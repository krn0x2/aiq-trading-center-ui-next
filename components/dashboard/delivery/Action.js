import React from 'react';
import { Button, Row, Icon } from 'antd';

export default () => (
  <div>
    <Row style={{ margin: '5px 0' }}><Button size="small" shape="circle" icon="pushpin" /></Row>
    <Row style={{ margin: '5px 0' }}><Button size="small" shape="circle" icon="caret-up" /></Row>
    <Row style={{ margin: '5px 0' }}><Button size="small" shape="circle" icon="delete" /></Row>
  </div>
);
