import React from 'react';
import { Row, Col } from 'antd';
import ProgressBar from '../../common/ProgressBar';
import { truncate, localNumber } from '../../../utils';

export default (text, c) => {
  const { currency, deliveredImpressions,
    bookedImpressions, percentImpressionDelivery,
    remainingImpressions, yesterdayImpressions, daysRemaining } = c;
  return (
    <div>
      <Row>
        <Col span={12}>
          <Row>Del. {localNumber(deliveredImpressions, currency)}</Row>
          <Row>Bkd. {localNumber(bookedImpressions, currency)}</Row>
          <Row>Rem. {localNumber(remainingImpressions, currency)}</Row>
        </Col>
        <Col span={12}>
          <Row span={12}>Yest. {localNumber(yesterdayImpressions, currency)}</Row>
          <Row span={12}>Req. {localNumber((remainingImpressions / daysRemaining),
            currency)}/day</Row>
        </Col>
      </Row>
      <ProgressBar percent={truncate(percentImpressionDelivery, 0)} strokeWidth={5} />
    </div>
  );
};
