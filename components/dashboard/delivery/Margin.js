import React from 'react';
import { Row } from 'antd';
import { localNumber, localCurrency } from '../../../utils';

export default (text, c) => {
  const { currency, margin, profit } = c;
  return (
    <div>
      <Row>{localNumber(margin, currency)} %</Row>
      <Row>Profit</Row>
      <Row>( {localCurrency(profit, currency)} )</Row>
    </div>
  );
};
