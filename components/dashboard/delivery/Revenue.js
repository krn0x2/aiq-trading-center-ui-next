import React from 'react';
import { Row } from 'antd';
import ProgressBar from '../../common/ProgressBar';
import { localCurrency, truncate } from '../../../utils';

export default (text, c) => {
  const { deliveredRevenue, bookedRevenue, currency,
    percentDeliveredRevenue, remainingRevenue } = c;
  return (
    <div>
      <Row>Del. {localCurrency(deliveredRevenue, currency)}</Row>
      <Row>Bkd. {localCurrency(bookedRevenue, currency)}</Row>
      <Row>Rem. {localCurrency(remainingRevenue, currency)}</Row>
      <ProgressBar percent={truncate(percentDeliveredRevenue, 0)} strokeWidth={5} />
    </div>
  );
};
