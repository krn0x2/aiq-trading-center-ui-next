import React from 'react';
import { Row, Col } from 'antd';
import { localCurrency } from '../../../utils';

export default (text, c) => {
  const { currency, mediaSpend, totalSpend, yesterdayMediaSpend,
    yesterdayTotalSpend, requiredMediaSpend } = c;
  return (
    <div>
      <Row>
        <Col span={12}>Del. {localCurrency(mediaSpend, currency)}</Col>
        <Col span={12}>Del. {localCurrency(totalSpend, currency)}</Col>
      </Row>
      <Row>
        <Col span={12}>Yest. {localCurrency(yesterdayMediaSpend, currency)}</Col>
        <Col span={12}>Yest. {localCurrency(yesterdayTotalSpend, currency)}</Col>
      </Row>
      <Row>Req. {localCurrency(requiredMediaSpend, currency)}/day</Row>
    </div>
  );
};
