import React from 'react';
import { Icon, Row, Col } from 'antd';
import Link from 'next/link';
import { formatDate, localCurrency } from '../../../utils';

export default (text, c) => {
  const { currency, grossBudget, netBudget } = c;
  return (
    <div>
      <Row><Link href={`campaign/${c.id}`}><a>{c.name}</a></Link></Row>
      <Row>
        <span>{formatDate(c.startDate)}</span>
        <Icon type="arrow-right" />
        <span>{formatDate(c.endDate)}</span>
      </Row>
      <Row>
        <Col span={12}>{localCurrency(grossBudget, currency)} (G)</Col>
        <Col span={12}>{localCurrency(netBudget, currency)} (N)</Col>
      </Row>
    </div>
  );
};
