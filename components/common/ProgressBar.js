import React from 'react';
import { Progress } from 'antd';

const ProgressBar = (props) => {
  const { percent } = props;
  return (<Progress {...props} percent={Math.min(percent, 100)} />);
};

export default ProgressBar;
