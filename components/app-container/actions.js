import * as ActionTypes from './actionTypes';
import { getAuthToken, redirectToAuthServer } from '../../utils/Session';

export const initiateUserAuth = () => (dispatch) => {
  const userToken = getAuthToken();
  if (!userToken) { redirectToAuthServer(); } else { dispatch({ type: ActionTypes.USER_AUTHENTICATED }); }
};

export const initiateUserLogout = () => ({
  type: ActionTypes.AUTH_LOGOUT,
  payload: {
    endpoint: 'user-session',
  },
});

