import React from 'react';
import { Layout, Icon, LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import JarvisHeader from './Header';

const { Content, Footer } = Layout;

const JarvisLayout = ({ children }) => (
  <LocaleProvider locale={enUS}>
    <Layout>
      <JarvisHeader />
      <Content style={{ padding: '0 50px', marginTop: 64, zIndex: 0 }}>
        <div style={{ background: '#fff', padding: 24, minHeight: 380 }}>
          {children}
        </div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>
        Jarvis ©2017 Created with <Icon type="heart" /> by Squad7
      </Footer>
    </Layout>
  </LocaleProvider>
);

export default JarvisLayout;
