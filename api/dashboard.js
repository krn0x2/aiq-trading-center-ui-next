import fetch from './api';

export default () => fetch(
  '/trading/api/dash/getCampaignList',
  'GET',
  {
    currency: 'Campaign_Currency',
    filterList: [],
    month: 7,
    orderList: [{ name: 'isPriority', type: 'DESC' }, { name: 'daysRemaining', type: 'ASC' }],
    page: 0,
    size: 50,
    viewMode: 'Month',
    year: 2017,
  },
);
