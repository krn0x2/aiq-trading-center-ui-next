# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Trading center app with SSR, tree-shaking and Esnext.

### How do I get set up? ###
	Install yarn/npm
	
	With yarn
	$ yarn
	$ yarn run production
	
	With npm
	$ npm install
	$ npm run production
	
	Visit => http://localhost:3000/

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact