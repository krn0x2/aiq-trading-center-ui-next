export const campaignStatus = [
  {
    name: 'All',
    value: null,
  },
  {
    name: 'Live',
    value: 'Live',
  },
  {
    name: 'Open',
    value: 'Open',
  },
  {
    name: 'Closed',
    value: 'Closed',
  },
  {
    name: 'Invoiced',
    value: 'Invoiced',
  },
  {
    name: 'Not Spending',
    value: 'NotSpending',
  },
];

export const currencies = ['USD', 'EUR', 'GBP', 'CAD', 'AUD', 'NZD', 'IDR', 'SGD', 'CHF', 'Campaign_Currency'];

export const currencySymbols = {
  USD: '$',
  EUR: '€',
  GBP: '£',
  CAD: 'C$',
  AUD: 'A$',
  NZD: 'NZ$',
  IDR: 'Rp',
  SGD: 'S$',
  CHF: 'CHF',
};

const sortByList = [
  {
    id: 1,
    group: 'Monthly Bkd. Revenue',
    children: [
      {
        text: 'Monthly Bkd. Revenue: High to Low',
        rule: {
          name: 'bookedRevenue',
          type: 'DESC',
        },
      },
      {
        text: 'Monthly Bkd. Revenue: Low to High',
        rule: {
          name: 'bookedRevenue',
          type: 'ASC',
        },
      },
    ],
  },
  {
    id: 2,
    group: 'Margin',
    children: [
      {
        text: 'Margin: High to Low',
        rule: {
          name: 'margin',
          type: 'DESC',
        },
      },
      {
        text: 'Margin: Low to High',
        rule: {
          name: 'margin',
          type: 'ASC',
        },
      },
    ],
  },
  {
    id: 3,

    group: 'Days Remaining',
    children: [
      {
        text: 'Ending Soon',
        rule: {
          name: 'daysRemaining',
          type: 'ASC',
        },
      },
    ],
  },
  {
    id: 4,
    group: 'Campaign Name',
    children: [
      {
        text: 'Campaign Name: A-Z',
        rule: {
          name: 'name',
          type: 'ASC',
        },
      },
      {
        text: 'Campaign Name: Z-A',
        rule: {
          name: 'name',
          type: 'DESC',
        },
      },
    ],
  },
  {
    id: 5,
    group: 'Required spend',
    children: [
      {
        text: 'Daily Req. Spend: High to Low',
        rule: {
          name: 'requiredMediaSpend',
          type: 'DESC',
        },
      },
      {
        text: 'Daily Req. Spend: Low to High',
        rule: {
          name: 'requiredMediaSpend',
          type: 'ASC',
        },
      },
    ],
  },
];

export default sortByList;
