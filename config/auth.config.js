/**
* The auth server configuration.
*/
export const authServerConfig = {
  hostname: 'accounts.mediaiqdigital.com',
  protocol: 'https',
};

  /**
  * This is used for storing the auth token in cookies.
  * For cookie we would use instance specific cookie key.
  * This cookie key is for auth in development.
  */
export const AUTH_TOKEN_NAME = 'token';

