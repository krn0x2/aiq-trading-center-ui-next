const mode = {
  development: {
    BASE_URL: '//localhost.mediaiqdigital.com/tc',
    AUTH_URL: '//accounts.mediaiqdigital.com',
  },
  staging: {
    BASE_URL: '//jarvis-staging.mediaiqdigital.com/tc',
    AUTH_URL: '//accounts.mediaiqdigital.com',
  },
  production: {
    BASE_URL: '//jarvis.mediaiqdigital.com',
    AUTH_URL: '//accounts.mediaiqdigital.com',
  },
};
const config = mode[process.env.NODE_ENV];
const configKeys = Object.keys(config);
const newConfig = configKeys.reduce((partial, key) => {
  partial[`process.env.${key}`] = config[key];
  return partial;
}, {});
module.exports = newConfig;
