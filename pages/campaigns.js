import React from 'react';
import Link from 'next/link';
import { Radio, Select, Table, Row, Col, DatePicker, Button, Tabs, Input } from 'antd';
import getCampaigns from '../api/dashboard';
import Layout from '../components/Layout';

import { Info, Impressions, Revenue, Spend, Margin, Action } from '../components/dashboard/delivery';
import sortByList, { currencies, campaignStatus } from '../constants';

const { MonthPicker } = DatePicker;
const TabPane = Tabs.TabPane;
const ButtonGroup = Button.Group;
const Search = Input.Search;
const { Option, OptGroup } = Select;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;


const deliveryColumns = [{
  title: 'Campaign',
  render: Info,
  width: 300,
}, {
  title: <div>
    <span>Impressions </span>
    <RadioGroup onChange={() => {}} defaultValue="client">
      <RadioButton value="client">Client</RadioButton>
      <RadioButton value="dsp">DSP</RadioButton>
    </RadioGroup></div>,
  render: Impressions,
  width: 246,
}, {
  title: 'Revenue',
  render: Revenue,
  width: 227,
}, {
  title: 'Media Spend/Total Spend',
  render: Spend,
  width: 201,
}, {
  title: 'Margin',
  render: Margin,
  width: 108,
}, {
  render: Action,
  width: 40,
}];

const performanceColumns = [{
  title: 'Campaign',
  render: Info,
  width: 300,
}, {
  render: Action,
  width: 40,
}];

const operations = (
  <div>
    <Search
      placeholder="Search campaign name"
      style={{ width: 250 }}
      onSearch={value => console.log(value)}
    />
    <Select
      showSearch
      style={{ width: 150 }}
      placeholder="Status"
      optionFilterProp="children"
      onChange={() => {}}
      onFocus={() => {}}
      onBlur={() => {}}
      filterOption={(input, option) =>
        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
    >
      {
        campaignStatus.map(status =>
          (<Option
            key={status.name}
            value={status.value}
          >{status.name}</Option>))
      }
    </Select>
    <Select
      showSearch
      style={{ width: 150 }}
      placeholder="Currency"
      optionFilterProp="children"
      onChange={() => {}}
      onFocus={() => {}}
      onBlur={() => {}}
      filterOption={(input, option) =>
        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
    >
      {
        currencies.map(currency => <Option key={currency} value={currency}>{currency}</Option>)
      }
    </Select>
    <Select
      style={{ width: 150 }}
      placeholder="Sort By"
      optionFilterProp="children"
      onChange={() => {}}
    >
      {
        sortByList.map(grp => (
          <OptGroup key={grp.id} label={grp.group}>
            {
              grp.children.map(sort =>
                (<Option
                  key={JSON.stringify(sort.rule)}
                  value={JSON.stringify(sort.rule)}
                >{sort.text}</Option>))
            }
          </OptGroup>
        ))
      }
    </Select>
  </div>
);

const Campaigns = (props) => {
  const { campaigns } = props;
  return (
    <Layout>
      <Row gutter={10} style={{ marginBottom: '10px' }}>
        <Col span={8}>ALL CAMPAIGNS ({campaigns.length})</Col>
        <Col span={9}>
          Show campiagns for:
          <MonthPicker
            format="MMMM YYYY"
            onChange={() => {}}
            placeholder="Select month"
          />
        </Col>
        <Col span={4}>
          <ButtonGroup>
            <Button>Monthly</Button>
            <Button type="primary">Lifetime</Button>
          </ButtonGroup>
        </Col>
        <Col span={2}><Button type="primary">Create</Button></Col>
      </Row>
      <Tabs defaultActiveKey="1" tabBarExtraContent={operations}>
        <TabPane tab="Delivery" key="1">
          <Table
            rowKey="id"
            columns={deliveryColumns}
            dataSource={campaigns}
            bordered
            pagination={{ pageSize: 50 }}
            scroll={{ y: 600 }}
          />
        </TabPane>
        <TabPane tab="Performance" key="2">
          <Table
            rowKey="id"
            columns={performanceColumns}
            dataSource={campaigns}
            bordered
            pagination={{ pageSize: 50 }}
            scroll={{ y: 600 }}
          />
        </TabPane>
      </Tabs>
    </Layout>
  );
};

Campaigns.getInitialProps = async () => {
  console.log('i am here');
  const response = await getCampaigns();
  let allCampaigns = [];
  if (response && response.dtos) {
    allCampaigns = response.dtos.map(campaign => ({
      ...campaign.campaignInfo,
      id: campaign.id.id ? campaign.id.id : campaign.id,
      campaignGoals: JSON.parse(campaign.campaignInfo.campaignGoals),
    }));
  }
  return { campaigns: allCampaigns };
};

export default Campaigns;
