import React from 'react';
import { Layout, Icon, LocaleProvider } from 'antd';
import Document, { Head, Main, NextScript } from 'next/document';
import { ServerStyleSheet } from 'styled-components';
import enUS from 'antd/lib/locale-provider/en_US';
import JarvisHeader from '../components/Header';

const { Content, Footer } = Layout;

export default class MyDocument extends Document {
  render() {
    const sheet = new ServerStyleSheet();
    const main = sheet.collectStyles(<Main />);
    const styleTags = sheet.getStyleElement();
    return (
      <html lang="en-US">
        <Head>
          <link rel="stylesheet" href="/static/styles.css" />
          <title>Jarvis</title>
          {styleTags}
        </Head>
        <body>
          <div className="root">
            {main}
          </div>
          <NextScript />
        </body>
      </html>
    );
  }
}
